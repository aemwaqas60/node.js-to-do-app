//require mongoose module
const mongoose = require("mongoose");

//require database URL from envirnment file
const config= require("./config/config");

console.log('mongo url', config.mongoConfig.dbUrl);
mongoose
    .connect(config.mongoConfig.dbUrl, {useNewUrlParser: true})
    .then(res => {
        console.log(`Mongoose default connection is open to: ${config.mongoConfig.dbUrl}`);
    }).catch((err) => {
    console.log(`Mongoose connection error:  ${err}`);
});


module.exports = mongoose;
