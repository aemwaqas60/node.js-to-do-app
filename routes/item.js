const express = require("express");
const router = express.Router();
const controller_item = require("../app/controllers/item");

router.get("/", controller_item.findAll);
router.get("/:itemId", controller_item.findById);
router.post("/",controller_item.add);
router.put("/", controller_item.update);
router.delete("/", controller_item.remove);

module.exports = router;
