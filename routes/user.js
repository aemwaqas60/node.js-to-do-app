const express = require("express");
const router = express.Router();
const controllerUser = require("../app/controllers/user");

router.get("/", controllerUser.findAll);
router.get("/:userId", controllerUser.findById);
router.post("/", controllerUser.add);
router.put("/", controllerUser.update);
router.delete("/", controllerUser.remove);

module.exports = router;
