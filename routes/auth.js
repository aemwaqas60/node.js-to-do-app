const express = require("express");
const router = express.Router();
const controllerAuth = require("../app/controllers/auth");

router.post("/login",controllerAuth.login);

module.exports = router;
