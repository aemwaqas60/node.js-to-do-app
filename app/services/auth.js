const UserStore = require("../stores/user")
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const config = require("../../config/config");
const UserEntity = require("../entities/UserEntity");

const authService = {};

authService.login = async (user) => {
    try {
        const userObj = await authService.matchEmail(user);
        await authService.matchPassword(user, userObj);

        const token = jwt.sign({userId: userObj.userId}, config.JWT_SECRET);

        const loginUser = UserEntity.createFromUserObject(userObj).toString();
        loginUser.token = token;
        return loginUser;

    } catch (err) {
        throw err;
    }

};

authService.matchPassword = async (user, userObj) => {

    const error = new Error("Invalid username or password error!");
    error.status = 401;

    const match = await bcrypt.compare(user.password, userObj.password);
    if (match) return;

    throw error;
};


authService.matchEmail = async (user) => {

    const error = new Error("Invalid username or password error!");
    error.status = 401;

    const userObj = await UserStore.findByEmail(user);
    if (userObj === null)
        throw error;

    if (user.email === userObj.email) ;
    return userObj;

    throw error;

};

module.exports = authService;