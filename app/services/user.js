const UserStore = require("../stores/user");
const UserEntity = require('../entities/UserEntity');
const bcrypt = require('bcrypt');

const userService = {};
/**
 *
 * @param userObj
 * @returns {Promise<*>}
 */
userService.add = async (userObj) => {
    try {

        const salt = await bcrypt.genSalt(10);
        userObj.password = await bcrypt.hash(userObj.password, salt);


        const user = await UserEntity.createUserDetails(
            userObj.firstName,
            userObj.lastName,
            userObj.userName,
            userObj.email,
            userObj.password
        );
        return await UserStore.add(user);
    } catch (err) {
        console.log("err inside service:  ", err)
        throw err;
    }
};

/**
 *
 * @returns {Promise<*>}
 */
userService.findAll = async () => {

    try {
        return await UserStore.findAll();
    } catch (err) {
        throw err;
    }
};

/**
 *
 * @param userId
 * @returns {Promise<void>}
 */
userService.findById = async (userId) => {
    try {
        return await UserStore.findById(userId);

    } catch (err) {
        throw err;
    }

};

/**
 *
 * @param user
 * @returns {Promise<*>}
 */

userService.update = async (user) => {
    try {

        console.log("user inside service:", user)

        const userObj = await UserEntity.createFromUserObject(user);

        console.log("userObj inside service:", userObj)
        return await UserStore.update(userObj);

    } catch (err) {
        throw err;
    }
};

/**
 *
 * @param user
 * @returns {Promise<boolean>}
 */
userService.remove = async (user) => {
    try {
        await UserStore.remove(user);
        return true;

    } catch (err) {
        throw err;
    }

};


module.exports = userService;