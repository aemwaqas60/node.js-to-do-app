const ItemStore = require("../stores/item");
const ItemEntity = require('../entities/ItemEntity');

const itemService = {};


/**
 *
 * @param item
 * @returns {Promise<ItemEntity>}
 */
itemService.add = async (item) => {
    try {
        let itemObj = await ItemEntity.createItemDetails(
            item.itemName,
            item.itemType
        );
        return await ItemStore.add(itemObj);
    } catch (err) {
        throw err;
    }
};
/**
 *
 * @returns {Promise<*>}
 */
itemService.findAll = async () => {
    try {
        return await ItemStore.findAll();
    } catch (err) {
        throw err;
    }
};
/**
 *
 * @param itemId
 * @returns {Promise<ItemEntity>}
 */
itemService.findById = async (itemId) => {
    try {
        return await ItemStore.findById(itemId);
    } catch (err) {
        throw err;
    }
};
/**
 *
 * @param item
 * @returns {Promise<ItemEntity>}
 */
itemService.update = async (item) => {
    try {
        console.log("Inside server item: ", item)

        let itemObj = await ItemEntity.createFromItemObject(item);
        return await ItemStore.update(itemObj);

    } catch (err) {
        throw err;
    }
};

/**
 *
 * @param item
 * @returns {Promise<boolean>}
 */
itemService.remove = async (item) => {
    try {
        await ItemStore.remove(item);
        return true;

    } catch (err) {
        throw err;
    }

};


module.exports = itemService;
