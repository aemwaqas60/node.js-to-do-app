const uuidv4 = require('uuid/v4');

class UserEntity {
    constructor(userId,firstName, lastName, userName,email,password) {
        this.userId= userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = userName;
        this.email = email;
        this.password=password;
    }



    static createUserDetails(firstName,lastName,userName,email,password) {

         return new UserEntity(uuidv4(),firstName, lastName, userName,email,password);
    }

    static createFromUserObject(obj) {
        return  new UserEntity(obj.userId,obj.firstName, obj.lastName, obj.userName,obj.email,obj.password);

    }

    toString(){
        return {
            userId:this.userId,
            firstName: this.firstName,
            lastName:this.lastName,
            userName:this.userName,
            email:this.email
        }
    }
}


module.exports = UserEntity;
