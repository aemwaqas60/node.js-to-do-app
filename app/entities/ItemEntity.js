const uuidv4 = require("uuid/v4");


class ItemEntity {
    constructor(itemId, itemName, itemType) {
        this.itemId = itemId;
        this.itemName = itemName;
        this.itemType = itemType;
    }

    static createItemDetails(itemName, itemType) {

        return new ItemEntity(uuidv4(), itemName, itemType);
    }

    static createFromItemObject(obj) {
        return new ItemEntity(obj.itemId, obj.itemName, obj.itemType);
    }



}

module.exports = ItemEntity;
