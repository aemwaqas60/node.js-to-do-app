const authService = require("../services/auth");

const authController = {};

authController.login = async (req, res, next) => {
    try {
        const user = await authService.login(req.body);
        res.status(200).json({message: "success", user: user})
        console.log("user : ", user);

    } catch (err) {
        next(err);
    }

}

module.exports = authController;


