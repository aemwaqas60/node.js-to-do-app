const serviceUser = require("../services/user");

const userController = {};
/**user
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
userController.add = async (req, res, next) => {
    try {
        const user = await serviceUser.add(req.body);
        res.status(200).json({message: "success", data: user});
    } catch (err) {
        next(err);
    }
};

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
userController.findAll = async (req, res, next) => {
    try {

        const users = await serviceUser.findAll();
        res.status(200).json({message: "success", data: users});
    } catch (err) {
        next(err);
    }
};

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
userController.findById = async (req, res, next) => {
    try {
        const user = await serviceUser.findById(req.params.userId);
        res.status(200).json({message: "success", data: user});

    } catch (err) {
        next(err);
    }
};

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
userController.update = async (req, res, next) => {

    try {
        const user = await serviceUser.update(req.body)

        res.status(200).json({message: "success!", data: user});
    } catch (err) {
        next(err);
    }
};

/**
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
userController.remove = async (req, res, next) => {
    try {
        // console.log("User object:",req.body.userId)

        await serviceUser.remove(req.body);

        res.status(200).json({message: "success"});

    } catch (err) {
        next(err);
    }
};


module.exports = userController;