const itemService = require("../services/item");

const itemController = {};

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
itemController.add = async (req, res, next) => {

    try {

        const item = await itemService.add(req.body);
        res.status(200).json({message: "success", data: item});
    } catch (err) {
        next(err);
    }
};

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
itemController.findAll = async (req, res, next) => {
    try {
        const items = await itemService.findAll();

        res.status(200).json({
            message: "success", data: items
        });
    } catch (err) {
        next(err);
    }
};
/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
itemController.findById = async (req, res, next) => {
    try {
        const itemId = req.params.itemId;
        const item = await itemService.findById(itemId);

        res.status(200).json({message: "success", data: item});
    } catch (err) {
        next(err);
    }
};

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
itemController.update = async (req, res, next) => {
    try {
        const item = await itemService.update(req.body);
        res.status(200).json({message: "success!", data: item});
    } catch (err) {
        next(err);
    }
};

/**
 *
 * @param req
 * @param res
 * @param next
 * @returns {Promise<void>}
 */
itemController.remove = async (req, res, next) => {
    try {
        await itemService.remove(req.body);
        res.status(200).json({message: "success"});
    } catch (err) {
        next(err);
    }
};


module.exports = itemController;