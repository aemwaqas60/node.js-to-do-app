const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const ItemEnity = require("../entities/ItemEntity");

const ItemSchema = new Schema({
    itemId:
        {
            type: String,
            required: true,
            unique: true
        },
    itemName: {
        type: String,
        default: null
    },
    itemType: {
        type: String,
        default: null
    },
    createdDate: {
        type: Date,
        default: Date.now
    }
});

const itemModel = mongoose.model("Item", ItemSchema);

class ItemStore {

    /**
     *
     * @param item
     * @returns {Promise<ItemEntity>}
     */
    static async add(item) {
        try {
            const itemObj = await itemModel.create(item);
            return ItemEnity.createFromItemObject(itemObj);
        } catch (err) {
            const error = Error("Unable to create new item record.");
            error.status = 500;
            throw error;
        }
    }

    /**
     *
     * @returns {array[objects]}
     */
    static async findAll() {
        try {
            const items = await itemModel.find({}).exec();
            return items.map(item => ItemEnity.createFromItemObject(item));
        } catch (err) {
            const error = Error("Unable to get items data.");
            error.status=404;
            throw error;
        }
    }

    /**
     *
     * @param itemId
     * @returns {Promise<ItemEntity>}
     */
    static async findById(itemId) {
        try {
            const item = await itemModel.findOne({itemId}).exec();
            return ItemEnity.createFromItemObject(item);
        } catch (err) {
            const error = Error("Unable to get item data.");
            error.status= 404;
            throw error;
        }
    }


    /**
     *
     * @param item
     * @returns {Promise<ItemEntity>}
     */
    static async update(item) {
        try {
            const itemId = item.itemId;
            const itemObj = await itemModel.findOneAndUpdate({itemId}, item, {new:true}).exec();
            return ItemEnity.createFromItemObject(itemObj);

        } catch (err) {

            const error = Error("Unable to update item record");
            error.status= 404;
            throw error;
        }
    }

    /**
     *
     * @param item
     * @returns {Promise<boolean>}
     */
    static async remove(item) {
        try {
            const itemId = item.itemId;
            await itemModel.deleteOne({itemId});
            return true;
        } catch (err) {
            const error = Error("Unable to remove item record.");
            error.status= 404;
            throw error;
        }
    }
}

module.exports = ItemStore;
