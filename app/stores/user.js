const mongoose = require("mongoose");
const Schema = mongoose.Schema;
const UserEntity = require("../entities/UserEntity");

const UserSchema = new Schema({
    userId: {
        type: String,
        required: true,
        unique: true
    },
    firstName: {
        type: String,
        default: null
    }
    ,
    lastName: {
        type: String,
        default: null
    }
    ,
    userName: {
        type: String,
        default: null
    }
    ,
    email: {
        type: String,
        required: true,
        unique: true
    }
    ,
    password: {
        type: String,
        required: true,
    }
    ,
    createdDate: {
        type: Date,
        default: Date.now
    }
});
const userModel = mongoose.model("User", UserSchema);

class UserStore {

    /**
     *
     * @param {object} user object
     * @returns {object} new user object
     */
    static async add(user) {
        try {
            const userObj = await userModel.create(user);
            return UserEntity.createFromUserObject(userObj);

        } catch (err) {

            const error = new Error("unable to add new user");
            error.status = 409;
            throw  error;
        }
    }

    /**
     *
     * @param {null}
     * @returns {array[object]} array of users objects
     */
    static async findAll() {
        try {
            const users = await userModel.find({}).exec();
            return users.map(user => UserEntity.createFromUserObject(user).toString());
        } catch (err) {
            const error = new Error("Unable to find users data.");
            error.status = 404;
            throw  error;
        }
    }

    /**
     *
     * @param {string} userId
     * @returns {user} user object
     */
    static async findById(userId) {
        try {
            const userObj = await userModel.findOne({userId}).exec();
            return UserEntity.createFromUserObject(userObj).toString();
        } catch (err) {
            const error = new Error("Unable to find user data.");
            error.status = 404;
            throw  error;
        }
    }

    /**
     *
     * @param user
     * @returns {Promise<void>}
     */
    static async findByEmail(user) {
        try {

            return await userModel.findOne({email: user.email});

        } catch (err) {
            throw err;
        }
    }

    /**
     *
     * @param {object} user object
     * @returns {object} updated user object
     */
    static async update(user) {
        try {
            console.log("User data in store: ", user)
            const userInstance = await userModel.findOneAndUpdate({userId: user.userId}, user, {new: true});
            return UserEntity.createFromUserObject(userInstance).toString();
        } catch (err) {
            const error = new Error("Unable to update user data.");
            error.status = 404;
            throw  error;
        }
    }

    /**
     *
     * @param {object} user object
     * @returns {boolean} true or false
     */
    static async remove(user) {
        try {
            const userId = user.userId;
            await userModel.deleteOne({userId});
            return true;
        } catch (err) {

            const error = new Error("Unable to delete user.");
            error.status = 404;
            throw  error;
        }
    }
}

module.exports = UserStore;
