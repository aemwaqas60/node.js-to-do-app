const express = require("express");
const logger = require("../app/middleware/logger");
const app = express();
const morgan = require("morgan");


//Express middleware for parsing json data
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// static files path
app.use(express.static("public"));

//View Engine Settings
app.set("view engine", "pug");
app.set("views", "./views");

// Middleware functions
app.use(logger);

//Middleware Morgan for logging Request on console
if (app.get("env") === "development") {
    app.use(morgan("dev"));
    console.log("Morgan Enabled....");
};



module.exports = app;