const app = require("./expressApp");
const routesUser = require("../routes/user");
const routesItems = require("../routes/item");
const routesAuth = require("../routes/auth");
// user routes
app.use("/api/v1/user", routesUser);
app.use("/api/v1/item", routesItems);
app.use("/api/v1/auth", routesAuth);


app.use(function (err, req, res) {
    res.status(err.status || 500);

    res.json({
        error: {
            message: err.message
        }
    });
});


module.exports = app;

